package com.docstorm.exceptions;

public class AccessNotPermittedException extends RuntimeException{

	public AccessNotPermittedException(String msg) {
		super(msg);
	}
}
