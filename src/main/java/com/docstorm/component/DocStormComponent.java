package com.docstorm.component;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.docstorm.dao.DaoReositoryForDocTransInfo;
import com.docstorm.dao.DaoRepository;
import com.docstorm.dao.DaoUsers;
import com.docstorm.dto.DocumentWithTransInfoDto;
import com.docstorm.dto.TransInfoDto;
import com.docstorm.exceptions.AccessNotPermittedException;
import com.docstorm.model.DocTransInfo;
import com.docstorm.model.Document;
import com.docstorm.model.Users;
import com.docstorm.pojos.SetAndGetStatus;
import com.docstorm.representation.DocRepresentation;

@Component
public class DocStormComponent {

	private static final Logger logger = LoggerFactory.getLogger(DocStormComponent.class);

	@Autowired
	DaoRepository daoRepository;
	@Autowired
	DaoReositoryForDocTransInfo daoReositoryForDocTransInfo;
	@Autowired
	DaoUsers daoUsers;
	

	public List<List<DocRepresentation>> getAllDocsWithTag(long tagId, String acceptLang, long size) {

		List<Document> doc = null;
		List<DocTransInfo> docTransInfo = null;
		try {

			doc = daoRepository.findAllByTag(tagId);

		} catch (Exception e) {
			logger.error("there is an error during retrieving from DB");
			throw e;
		}
		List<DocRepresentation> docRep = new ArrayList<DocRepresentation>();
		List <List<DocRepresentation>> docRepList = new ArrayList<List<DocRepresentation>>();
		int i = 1;
		for (Document d : doc) {
			//this check for size of items per page
			if (i <= size) {
				try {
					docTransInfo = daoReositoryForDocTransInfo.findDocsWithLangCode(acceptLang, d.getId());
				} catch (Exception e) {
					logger.error("there is an error during retrieving from DB");
					throw e;
				}
				d.setDocTransInfo(docTransInfo);
				docRep.add(d.getDocRepresentation());
				
			}
			if (i == size){
				docRepList.add(docRep);
				i = 0;
				
				docRep = new ArrayList<DocRepresentation>();
			}
			++i;
		}
		if (!docRep.isEmpty()){
			docRepList.add(docRep);
		}
		

		return docRepList;
	}

	public List<List<DocRepresentation>> getAllDocs(String acceptLang, long size) {

		List<Document> doc = null;
		List<DocTransInfo> docTransInfo = null;
		try {
			doc = daoRepository.findAll();
		} catch (Exception e) {
			logger.error("there is an during retrieving from DB");
			throw e;
		}
		List<DocRepresentation> docRep = new ArrayList<DocRepresentation>();
		List <List<DocRepresentation>> docRepList = new ArrayList<List<DocRepresentation>>();
		int i = 1;
		for (Document d : doc) {
			if (i <= size) {
				try {
					docTransInfo = daoReositoryForDocTransInfo.findDocsWithLangCode(acceptLang, d.getId());
				} catch (Exception e) {
					logger.error("there is an error during retrieving from DB");
					throw e;
				}
				d.setDocTransInfo(docTransInfo);
				docRep.add(d.getDocRepresentation());
				
			}
			if (i == size){
				docRepList.add(docRep);
				i = 0;
				
				docRep = new ArrayList<DocRepresentation>();
			}
			++i;
		}
		if (!docRep.isEmpty()){
			docRepList.add(docRep);
		}
			

		return docRepList;
	}

	public SetAndGetStatus createDocs(DocumentWithTransInfoDto documentWithTransInfoDto, String userName) {
		
		
		SetAndGetStatus setAndGetStatus = new SetAndGetStatus();
		Users users = null;
		try{
			//users = new Users();
			users = daoUsers.findOneByUserName(userName);
			
		}catch(Exception e){
			logger.error("there is an during retrieving from DB");
			throw e;
		}
		
		if (!users.getUserRole().equals("admin")){
			throw new AccessNotPermittedException("Access Not Allowed");
		}
		
		Document document = new Document();
		document.setTag(documentWithTransInfoDto.getTag());
		document.setCreateDate(documentWithTransInfoDto.getCreateDate());
		
		try {
			
			
			
			daoRepository.save(document);
			List<DocTransInfo> docTransInfos = documentWithTransInfoDto.getDocTransInfo();
			for (DocTransInfo dt : docTransInfos) {
				dt.setDocument_id(document.getId());
				daoReositoryForDocTransInfo.save(dt);
			}

			setAndGetStatus.setStatus("Succeeded");
		} catch (Exception e) {
			setAndGetStatus.setStatus("Failed");
			logger.error("there is an error during saving in DB");
			throw e;
		}
		return setAndGetStatus;

	}

	public SetAndGetStatus addTransToDocs(TransInfoDto transInfoDto, long docId, String userName) {
		
		
		SetAndGetStatus setAndGetStatus = new SetAndGetStatus();
		Users users = null;
		try{
			//users = new Users();
			users = daoUsers.findOneByUserName(userName);
			
		}catch(Exception e){
			logger.error("there is an during retrieving from DB");
			throw e;
		}
		
		if (!users.getUserRole().equals("admin")){
			throw new AccessNotPermittedException("Access Not Allowed");
		}
		
		DocTransInfo docTransInfo = new DocTransInfo();
		docTransInfo.setDocument_id(docId);
		docTransInfo.setBody(transInfoDto.getBody());
		docTransInfo.setLanguageCode(transInfoDto.getLanguageCode());
		docTransInfo.setName(transInfoDto.getName());
		
		try {
			
			daoReositoryForDocTransInfo.save(docTransInfo);
			setAndGetStatus.setStatus("Succeeded");

		} catch (Exception e) {
			setAndGetStatus.setStatus("Failed");
			logger.error("there is an error during saving in DB");
			throw e;
		}
		return setAndGetStatus;

	}
}
