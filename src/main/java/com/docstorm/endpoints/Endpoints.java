package com.docstorm.endpoints;

import java.util.List;

import javax.servlet.http.HttpServletRequest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.docstorm.component.DocStormComponent;
import com.docstorm.dto.DocumentWithTransInfoDto;
import com.docstorm.dto.TransInfoDto;
import com.docstorm.exceptions.AccessNotPermittedException;
import com.docstorm.pojos.SetAndGetStatus;
import com.docstorm.representation.DocRepresentation;

@RestController
@RequestMapping(path="/api/rest")
public class Endpoints {

	//private static final Logger logger = LoggerFactory.getLogger(Endpoints.class);

	@Autowired
	DocStormComponent docStormComponent;
	
	//@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@RequestMapping(value = "/tags/{tagId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
	public ResponseEntity<List<List<DocRepresentation>>> getDocumentsByTagId(@RequestParam (value="size", defaultValue="10") long size, @RequestHeader (value="AcceptLanguage" , defaultValue="us") String accLang, @PathVariable long tagId, HttpServletRequest httpRequest) {
		 
		
		List<List<DocRepresentation>> docs = docStormComponent.getAllDocsWithTag(tagId, accLang, size);

		return new ResponseEntity<List<List<DocRepresentation>>>(docs, HttpStatus.OK);
	}
	
	//@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@RequestMapping(value = "/documents", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
	public ResponseEntity<List<List<DocRepresentation>>> getAllDocuments(@RequestParam (value="size", defaultValue="10") long size, @RequestHeader (value="AcceptLanguage" , defaultValue="us") String accLang, HttpServletRequest httpRequest) {
		
		
		List<List<DocRepresentation>> docs = docStormComponent.getAllDocs(accLang, size);

		return new ResponseEntity<List<List<DocRepresentation>>>(docs, HttpStatus.OK);
	}
	
	
	//@Secured({"ROLE_ADMIN"})
	@RequestMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public ResponseEntity<SetAndGetStatus> createDocument(@RequestBody DocumentWithTransInfoDto documentWithTransInfoDto, HttpServletRequest httpRequest) {
		
		String userName = httpRequest.getHeader("userName");
		SetAndGetStatus setAndGetStatus = docStormComponent.createDocs(documentWithTransInfoDto, userName);
		return new ResponseEntity<SetAndGetStatus>(setAndGetStatus, HttpStatus.OK);
	}
	
	//@Secured({"ROLE_ADMIN"})
	@RequestMapping(value = "/documents/{docId}/trans", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public ResponseEntity<SetAndGetStatus> addTrans(@RequestBody TransInfoDto transInfoDto, @PathVariable long docId, HttpServletRequest httpRequest) {
		
		String userName = httpRequest.getHeader("userName");
		SetAndGetStatus setAndGetStatus = docStormComponent.addTransToDocs(transInfoDto, docId, userName);
		
		return new ResponseEntity<SetAndGetStatus>(setAndGetStatus, HttpStatus.OK);
	}
	
	@ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason = "10")
	@ExceptionHandler(value = AccessNotPermittedException.class)
	public void handleAccessNotPermittedException() {

	}
}
