package com.docstorm.representation;

import java.util.List;

import com.docstorm.model.DocTransInfo;

public class DocRepresentation {
	
	private long id;
	private long tag;
	private String createDate;
	private List <DocTransInfo> docTransInfo;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getTag() {
		return tag;
	}
	public void setTag(long tag) {
		this.tag = tag;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public List<DocTransInfo> getDocTransInfo() {
		return docTransInfo;
	}
	public void setDocTransInfo(List<DocTransInfo> docTransInfo) {
		this.docTransInfo = docTransInfo;
	}
	
	
}
