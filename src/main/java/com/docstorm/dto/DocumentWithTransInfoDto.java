package com.docstorm.dto;

import java.util.List;

import com.docstorm.model.DocTransInfo;

public class DocumentWithTransInfoDto {

	private long tag;
	private String createDate;
	private List <DocTransInfo> docTransInfo;
	
	public long getTag() {
		return tag;
	}
	public void setTag(long tag) {
		this.tag = tag;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public List<DocTransInfo> getDocTransInfo() {
		return docTransInfo;
	}
	public void setDocTransInfo(List<DocTransInfo> docTransInfo) {
		this.docTransInfo = docTransInfo;
	}
	
	
}
