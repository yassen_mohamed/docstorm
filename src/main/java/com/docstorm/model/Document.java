package com.docstorm.model;

import java.util.List;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import com.docstorm.representation.DocRepresentation;


@Entity
public class Document {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private String createDate;
	
	private Long tag;
	
	public Long getId() {
		return id;
	}
	@OneToMany
	@JoinColumn(name="document_id", referencedColumnName="id")
	private List <DocTransInfo> docTransInfo;
	
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public Long getTag() {
		return tag;
	}
	public void setTag(Long tag) {
		this.tag = tag;
	}
	
	
	public List<DocTransInfo> getDocTransInfo() {
		return docTransInfo;
	}
	public void setDocTransInfo(List<DocTransInfo> docTransInfo) {
		this.docTransInfo = docTransInfo;
	}
	
	public DocRepresentation getDocRepresentation(){
		
		DocRepresentation docRep = new DocRepresentation();
		
		docRep.setId(this.id);
		docRep.setTag(this.tag);
		docRep.setCreateDate(this.createDate);
		docRep.setDocTransInfo(this.docTransInfo);
		return docRep;
	}
	
}
