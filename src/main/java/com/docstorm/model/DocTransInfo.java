package com.docstorm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class DocTransInfo extends TransInfo{

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	

	//@ManyToOne
    @Column(name = "document_id")
	private Long document_id;
   
    
	public Long getDocument_id() {
		return document_id;
	}

	public void setDocument_id(Long document_id) {
		this.document_id = document_id;
	}

	

	
	
	
//	public Document getDocument() {
//		return document;
//	}
//
//	public void setDocument(Document document) {
//		this.document = document;
//	}
	
}
