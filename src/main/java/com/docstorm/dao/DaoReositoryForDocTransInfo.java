package com.docstorm.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.docstorm.model.DocTransInfo;

public interface DaoReositoryForDocTransInfo extends JpaRepository<DocTransInfo, Long> {

	public static final String FIND_PROJECTS = "SELECT id, body, language_code, name, document_id from doc_trans_info where language_code = :langCode and document_id = :docId";

	@Query(value = FIND_PROJECTS, nativeQuery = true)
	public List<DocTransInfo> findDocsWithLangCode(@Param("langCode") String langCode, @Param("docId") long docId);

	//List<DocTransInfo> findAllByLanguageCode(String langCode);
}
