package com.docstorm.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.docstorm.model.Users;

public interface DaoUsers extends JpaRepository<Users, Long>{
	
	Users findOneByUserName(String userName);
	
}
