package com.docstorm.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;


import com.docstorm.model.Document;

public interface DaoRepository extends JpaRepository<Document, Long>{
	
	List<Document> findAllByTag(long tagId);
	
//	public static final String FIND_PROJECTS = "SELECT doc.id, doc.create_date, doc.tag, trans.id, trans.body, trans.language_code, trans.name, trans.document_id from document doc inner join doc_trans_info trans on trans.language_code = :#{#langCode} ";
//
//	@Query(value = FIND_PROJECTS, nativeQuery = true)
//	public List<Document> findDocsWithLangCode(@Param("langCode") String langCode);
}
