package com.docstorm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DocStormApplication {

	public static void main(String[] args) {
		SpringApplication.run(DocStormApplication.class, args);
	}
}
