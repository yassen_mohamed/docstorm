package com.docstorm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.docstorm.component.DocStormComponent;
import com.docstorm.dao.DaoRepository;
import com.docstorm.dto.DocumentWithTransInfoDto;
import com.docstorm.dto.TransInfoDto;
import com.docstorm.model.DocTransInfo;
import com.docstorm.pojos.SetAndGetStatus;
import com.docstorm.representation.DocRepresentation;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DocStormApplicationTests {

	@Autowired
	DocStormComponent docStormComponent;
	
	@MockBean
	DaoRepository daoRepository;
	
	DocTransInfo docTransInfo;
	
	DocumentWithTransInfoDto documentWithTransInfoDto;
	TransInfoDto transInfoDto;
	
	@Before
	public void init(){
		docTransInfo = new DocTransInfo();
		documentWithTransInfoDto = new DocumentWithTransInfoDto();
		documentWithTransInfoDto.setTag(1l);
		documentWithTransInfoDto.setCreateDate("2017");
		docTransInfo.setDocument_id(1l);
		docTransInfo.setBody("hello, man");
		docTransInfo.setLanguageCode("us");
		docTransInfo.setName("yassen");
		
		List<DocTransInfo> docTransInfosList = new ArrayList<DocTransInfo>();
		docTransInfosList.add(docTransInfo);
		
		documentWithTransInfoDto.setDocTransInfo(docTransInfosList);
		transInfoDto = new TransInfoDto();
		transInfoDto.setLanguageCode("us");
		transInfoDto.setBody("nice , very good");
		transInfoDto.setName("mohamed");
		
		
	}
	
	@Test
	public void happyScenarioForGetAllDocsWithTag() {
		
		List <List<DocRepresentation>> docRepList = docStormComponent.getAllDocsWithTag(1l, "us", 10l);
		assertNotNull(docRepList);
	}

	@Test
	public void happyScenarioForGetAllDocs() {
		
		List <List<DocRepresentation>> docRepList = docStormComponent.getAllDocs("us", 10l);
		assertNotNull(docRepList);
	}
	
	
	@Test
	public void happyScenarioForCreateDocs() {
		
		SetAndGetStatus setAndGetStatus = docStormComponent.createDocs(documentWithTransInfoDto, "admin");
		assertEquals("Succeeded", setAndGetStatus.getStatus());
	}
	
	
	@Test
	public void happyScenarioForAddTransToDocs() {
		
		SetAndGetStatus setAndGetStatus = docStormComponent.addTransToDocs(transInfoDto, 1l, "admin");
		assertEquals("Succeeded", setAndGetStatus.getStatus());
	}
	
	
}
